# get_salt_grain.py (aka get-salt-grain)

Retrieves and displays the value of a given Salt grain for easy grep'ing in shell scripts.

# Usage

## Command line

To use `get_salt_grain.py`, run the script from the command line with the grain key you want to retrieve:

```
./get_salt_grain.py <grain_key>
```

Optionally, you can provide the path to the minion config file using the -c or --config flag:

```
./get_salt_grain.py -c /path/to/minion_config <grain_key>
```

### Examples

Retrieve the value of the `os` grain:

```
./get_salt_grain.py os
```

Retrieve the value of a nested grain, e.g., the 'hwaddr_interfaces' grain:

```
./get_salt_grain.py hwaddr_interfaces
```

# Dependencies

* Python 3.6 or higher
* Salt

# Reasoning

`salt-call grains.get <grain>` seems to require write access to the Salt cache directory keeping a normal user from running it. `salt-call -g` lists all grains and does not write to the cache directory. So that means if you're just trying to grep for the existance of a grain from an unprivileged script you have to do something like one of the below examples. However all seem really noisy if called in-line from a shell script, so I broke out the functionality to the script in this repo.

```powershell
# Using PowerShell
salt-call -g --local --out json | ConvertFrom-Json -Depth 100 | Select-Object @{Name = "lo"; Expression = {$_.local.hwaddr_interfaces.lo}}
```

```bash
# Pipe to Python from your shell
salt-call -g --local --out json | python3 -c "import sys, json; data=json.load(sys.stdin); print(data['local']['hwaddr_interfaces']['lo'])"
```

```bash
# Pipe to `jq` from your shell
salt-call -g --local --out json | jq '.local.hwaddr_interfaces.lo'
```