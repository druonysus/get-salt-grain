#!/usr/bin/env -S python3
"""
get_salt_grain.py

Retrieves and displays the value of a given Salt grain for easy grep'ing in shell scripts.

This file and its contents are distributed under the terms of the
Universal Permissive License, Version 1.0 (the "License").
ou may not use this file except in compliance with the License.

You may obtain a copy of the License in the included file,
LICENSES/UPL-1.0.txt or online at:

    https://opensource.org/license/upl

See the License for the specific language governing permissions and
limitations.

Copyright: 2023 Drew Adams <druonysus@opensuse.org>
License: UPL-1.0
Authors: Drew Adams
"""

import argparse
import salt.config
import salt.loader

class ArgumentParser:
    """
    A class for handling command-line argument parsing using argparse.
    """
    def __init__(self):
        """
        Initialize an ArgumentParser instance with the required arguments for the script.
        """
        self.parser = argparse.ArgumentParser(description="Retrieve and display the value of a given Salt grain for a minion.")
        self.parser.add_argument("grain_key", help="The key of the grain to retrieve.")
        self.parser.add_argument("-c", "--config", default="/etc/salt/minion", help="Path to the minion config file (default: /etc/salt/minion)")

    def parse(self):
        """
        Parse command-line arguments.

        Returns:
            argparse.Namespace: Parsed arguments including the grain_key and the config path.
        """
        return self.parser.parse_args()

class GrainLoader:
    """
    A class for handling Salt grain loading using the provided minion config path.
    """
    def __init__(self, config_path):
        """
        Initialize a GrainLoader instance with the provided minion config path.

        Args:
            config_path (str): Path to the minion config file.
        """
        self.config_path = config_path

    def load_grains(self):
        """
        Load Salt grains using the minion config path.

        Returns:
            dict: Loaded Salt grains.
        """
        __opts__ = salt.config.minion_config(self.config_path)
        return salt.loader.grains(__opts__)

class GrainValueGetter:
    """
    A class for handling the retrieval of Salt grain values from a given grains dictionary.
    """
    def __init__(self, grains):
        """
        Initialize a GrainValueGetter instance with the provided grains dictionary.

        Args:
            grains (dict): The Salt grains dictionary.
        """
        self.grains = grains

    def get_value(self, grain_key):
        """
        Retrieve the value of the specified grain key from the grains dictionary,
        handling nested data structures.

        Args:
            grain_key (str): The key of the grain to retrieve.

        Returns:
            Any: The value associated with the specified grain_key.
        """
        grain_value = self.grains
        for key in grain_key.split('.'):
            grain_value = grain_value.get(key)
            if grain_value is None:
                return None
        return grain_value

def print_grain_value(values):
    """
    Print the provided grain values in a simple, non-nested format, including nested data structures.

    Args:
        values (Any): The grain values to print, either a single value, a list or a nested dictionary.
    """
    if isinstance(values, list):
        for item in values:
            print(item)
    elif isinstance(values, dict):
        for key, value in values.items():
            print(f"{key}: {value}")
    else:
        print(values)

def main():
    """
    The main function of the script, responsible for executing the following steps:
    1. Parse command-line arguments.
    2. Load Salt grains using the provided (or default) minion config path.
    3. Retrieve the value for the specified grain key.
    4. Print the retrieved grain value in a simple, non-nested format.
    """
    arg_parser = ArgumentParser()
    args = arg_parser.parse()

    grain_loader = GrainLoader(args.config)
    grains = grain_loader.load_grains()

    grain_value_getter = GrainValueGetter(grains)
    grain_value = grain_value_getter.get_value(args.grain_key)

    print_grain_value(grain_value)


if __name__ == '__main__':
    main()
